//
//  ArtistasViewController.swift
//  Practica navegacion
//
//  Created by formador on 5/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class ArtistasViewController: UIViewController {

    var edad: Int {
        let edad = 16
        return edad
    }
    
    var name: String? {
        
        didSet{
            print("Nombre del artista \(name ?? "Hola"), edad \(edad)")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    

}
