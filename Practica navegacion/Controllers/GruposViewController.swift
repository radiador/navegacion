//
//  GruposViewController.swift
//  Practica navegacion
//
//  Created by formador on 5/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class GruposViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let button = view.viewWithTag(1) as? UIButton
        
        button?.addTarget(self, action: #selector(exitButtonTouched), for: UIControl.Event.touchUpInside)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ArtistasViewControlllerSegue" {
            
            let navigationController = segue.destination as? UINavigationController
            
            if let artistasViewController = navigationController?.topViewController as? ArtistasViewController {
                
                artistasViewController.name = "Pedro"
            }
        }
    }
    
    @objc func exitButtonTouched() {
        
        let discosViewController = DiscosViewController(nibName: "DiscosViewController", bundle: nil)
        
        discosViewController.delegado = self
        
        present(discosViewController, animated: true, completion: nil)
    }
}

extension GruposViewController: DiscosViewControllerDelegate {
   
    func requestCloseViewController() {
        
        dismiss(animated: true, completion: nil)
    }

}
