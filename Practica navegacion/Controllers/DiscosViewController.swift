//
//  DiscosViewController.swift
//  Practica navegacion
//
//  Created by formador on 5/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

protocol DiscosViewControllerDelegate {
    
    func requestCloseViewController()
}

class DiscosViewController: UIViewController {
    
    var delegado: DiscosViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func exitButtonAction(_ sender: Any) {
        
        delegado?.requestCloseViewController()
    }
    

}
