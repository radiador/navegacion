//
//  FichaViewController.swift
//  Practica navegacion
//
//  Created by formador on 5/3/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit

class FichaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func lanzaSegueButtonAction(_ sender: Any) {
        
        performSegue(withIdentifier: "muestraViewController", sender: self)
    }
    
    @IBAction func AnadeNavegacionButtonAction(_ sender: Any) {
        
        let whiteViewController = storyboard?.instantiateViewController(withIdentifier: "whiteViewControllerIdentifier")
        
        whiteViewController?.present(UIViewController(), animated: true, completion: nil)

        if let whiteViewController = whiteViewController {
            
            navigationController?.pushViewController(whiteViewController, animated: true)
            
        }
        
    }
    
    @IBAction func atrasButtonAction(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }
}
